// create array

// const array1 = ['eat', 'sleep'];

// console.log(array1);

// const array2 = new Array('pray', 'play');

// const newData = ['work', 1, true];




// // empty array

// const myList = [];

// // array numbers 




// const newData = [
// {'task1': 'exercise'},
// [1,2,3],
// function hello(){
// 	console.log('Hi I am array.')
// 	}

// // ];



// let places = ['Seoul', 'Tokyo', 'Palawan', 'Bali', 'Queenstown, New Zealand', 'Hobbiton, New Zealand', 'Paris' ];

// 	console.log(places[0]);
// 	console.log(places);
// 	console.log(places[places.length-1]);


// // array manipulation 
// // add elements to an array - push() and element at the end of the array

// let dailyActivities = ['eat', 'work', 'pray', 'play'];
// dailyActivities.push('exercise');
// console.log(dailyActivities);
// //unshift() and element at the beginning of the array
// dailyActivities.unshift('sleep');
// console.log(dailyActivities);

// dailyActivities[2] = 'sing'; // work will be replaced by sing
// console.log(dailyActivities);


// dailyActivities[6] = 'dance'; 
// console.log(dailyActivities);

// /*

// ACTIVITY

// */


// let places = ['Seoul', 'Tokyo', 'Palawan', 'Bali', 'Queenstown, New Zealand', 'Hobbiton, New Zealand', 'Paris' ];


// places.unshift('Barili');
// console.log(places);

// places.push('Pardo'); // adds Pardo in the last.
// console.log(places);

// console.log(places[0]);
// console.log(places[places.length-1]);




// adding items in an array wihtout using methods


// let array = [];
// console.log(array[0]);
// array[0] = 'Cloud Strife';
// console.log(array);
// console.log(array[1]);
// array[1] = 'Tifa Lockhart';
// console.log(array[1]);
// console.log(array);
// array[array.length-1] = 'Aerith Gainsborough'; // this will replace tifa lockhart;
// console.log(array);

// array[array.length] = 'Vincent Valentine'; // this will amend in the array, after aerith gainsborough
// console.log(array);

// array methods
// 	manipulate arrays with pre-determined js functions
// 	mutators - these arrays methods usually changed the original array.



// let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
// // without method
// array1[array.length] = 'Francisco'; // andres is replaced by francisco.
// console.log(array1);

// array1.unshift('Simon'); // place simon in [0]
// console.log(array1);

// // .pop() - allows us to delete or remove the llost item/element at the end of the array

// array1.pop();
// console.log(array1);

// // pop() is also able to return the item we remove
// console.log(array1.pop());
// console.log(array1);

// let removedItem = array1.pop();
// console.log(array1);
// console.log(removedItem);

// // .shift() return the item we removed

// let removedItemshift = array1.shift(); // removed the beginning element
// console.log(array1);
// console.log(removedItem);




// /*


// activity

// */

// let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];

// let removedItem = array1.shift();
// console.log(array1);
// console.log(removedItem);

// let removedItem2 = array1.pop();
// console.log(array1);
// console.log(removedItem2);

// array1.unshift('George');
// console.log(array1);

// array1.push('Michael');
// console.log(array1);


// // sort in ascending order

// array1.sort();
// console.log(array1);

// let numArray = [2, 3, 1, 6, 7, 9];
// numArray.sort();
// console.log(numArray);

// let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
// numArray2.sort((a,b) => a-b); // short function 
// console.log(numArray2);

//  // long function

// numArray2.sort(function(a,b) {  
// 		return (a-b);
// })

// console.log(numArray2);





// let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];

// arrayStr.sort();
// console.log(arrayStr);
// arrayStr.sort().reverse(); // sort in reverse
// console.log(arrayStr);



// // splice() - allows us to remove and add elements from a given index. syntax: array.splice(startingIndex, numberofItemstobeDeleted, elementstoAdd)

// let lakersPlayers = ['Lebron', 'Davis', 'Westbook', 'Kobe', 'Shaq'];

// console.log(lakersPlayers);

// lakersPlayers.splice(0, 0, 'Caruso');
// console.log(lakersPlayers);
// lakersPlayers.splice(0, 1);
// console.log(lakersPlayers);
// lakersPlayers.splice(0, 3);
// console.log(lakersPlayers);
// lakersPlayers.splice(1, 1);
// console.log(lakersPlayers);
// lakersPlayers.splice(1, 0, 'Gasol', 'Fisher');

// console.log(lakersPlayers);

// // non- mutators

// //slice() -

// let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];

// console.log(computerBrands);
// computerBrands.slice(2, 2, 'Compaq', 'Toshiba', 'Acer');

// console.log(computerBrands);


// // let color = ['Red', 'Blue', 'White', 'Violet', 'Pink', 'Black']

// // console.log(color);
// // let newColor = color.slice(1, 3);
// // console.log(newColor);


// /*

// activity

// */

// // let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];

// // let microsoft = videoGame.slice(3, 5);
// // console.log(microsoft);

// // let nintendo = videoGame.slice(2, 3);
// // console.log(nintendo);


// let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];

// let microsoft = videoGame.slice(3, 5);
// console.log(microsoft);

// let nintendo = videoGame.slice(2, 4);
// console.log(nintendo);


// .toString() - convert the array into a single value but each item will be separated by a comma
// syntax: array.toString()

// let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];

// let sentenceString = sentence.toString();
// console.log(sentence);
// console.log(sentenceString);

// // .join() - converts the array into a single value as a string but separater can be specified

// // syntax: array.join(separator)

// let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
// let sentenceString2 = sentence2.join(' ');
// console.log(sentenceString2);
// let sentenceString3 = sentence2.join("/");
// console.log(sentenceString3);


// acivity

// let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

// let name1Slice = charArr.slice(5,11)

// console.log(name1Slice)

// let name1 = name1Slice.join("")

// console.log(name1)

// let name2Slice = charArr.slice(13,19)


// let name2 = name2Slice.join("")

// console.log(name2)


// .contact() - it combines 2 or more arrays wihtout affecting the original 

// syntax: array.concat (array1, array2)


// let tasksFriday = ['drink HTML', 'eat JavaScript'];

// let tasksSaturday = ['inhale CSS', 'breath BootStrap']




// accessors 

	// methods that allow us to access our array.
	// indexof()
	// - finds the index of the given element/item when it is first founc from the left

	let batch131 = [
			'Paolo',
			'Jamir',
			'Jed',
			'Ronel',
			'Rom',
			'Jayson'

	];

// console.log(batch131)
	console.log(batch131.indexOf('Jed')); // 2
	console.log(batch131.indexOf('Rom')); //4


	//lastindexof()

			// - finds the index of the given element/item
			//	when it is last found from the right


console.log(batch131.lastIndexOf('Jamir')); //1 



/*

activity


*/



let carBrands = [
		'BMW',
		'dodge',
		'Maserati',
		'Porche',
		'chevrolet',
		'Ferrari',
		'GMC',
		'Porsche',
		'Mitsubishi',
	
];

function first(car) {
	return(car);
}

let firstCar = carBrands.indexOf('BMW')

console.log(firstCar);

function last(car2) {
	return (car2);
}


let lastCar = carBrands.lastIndexOf('Mitsubishi')

console.log(lastCar)


// answer to acivity


function indexFinder(brand) {
	console.log(carBrands.indexOf(brand));
}

function lastIndexFinder(brand) {
	console.log(carBrands.lastIndexOf(brand));
}


indexFinder('BMW');
lastIndexFinder('Porche');


// Iterator Methods
	// These methods iterate over the itemns in an array much like loop
	// However, with our iterator methods there also that allows to not only iterate over items but also additional instruction.

let avengers = [
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spider-man',
		'Iron Man',
		'Captain America'
];

// forEach() 
	//similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration
//Anonymous function within forEach will be receive each and evey item in an array

avengers.forEach(function(avenger){
	console.log(avenger);
});


let marvelHereos = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHereos.forEach(function(hero){
	// iterate over all the items in Marvel Heroes array and let them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// Add an if-else wherein Cyclops and Deadpool is not allow to join
		avengers.push(hero);
	}
});
console.log(avengers);


// map() - similar to forEach however it returns new array

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});
console.log(mappedNumbers);

// every()
	// iterates ovel all the items and checks if all the elements passess a given condition

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);
// some()
	// iterate over all the items check if even at least one of items in the array passes the condition. Same as every(), it will return boolean
let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});
console.log(checkForPassing);

// filter() - creates a new array that contains elements which passed a given condition

let numbersArr2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0;
});
console.log(divisibleBy5);



// find() - iterate over all items in our array but only returns the item that will satisfy the given condition

let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return username === 'mikeyTheKing2000';
});
console.log(foundUser);

// .includes() - returns a boolean true if it finds a matching item in the array. 
// Case-sensitive

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michealKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('sheJonesSmith@gmail.com');
console.log(doesEmailExist);



/* 


activity

/*
	Mini-Activity
	Create 2 functions 
		First Function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second Function is able to find a specified email already exist in the registeredEmails array.
			- IF  there is an email found, show an alert:
				"Email Already Exist."
			- IF there is no email found, show an alert:
				"Email is available, proceed to registration."

		You may use any of the three methods we discussed recently.

*/



let userNames = ['melz1', 'melz2', 'melz3', 'melz4'];


let gotUser = userNames.find(function(username) {
	console.log(username);
	return username == 'melz3';
})

console.log(gotUser);


let emails = ['melz1@gmail.com', 'melz2@yahoo.com', 'melz3@hotmail.com', 'melz4@mail.com'];



let testedEmails = emails.includes('test@gmail.com');

if (testedEmails == true) {
	alert("Email already exist");

} else {
	alert("Email is available, proceed to registration")
}















